//
//  ViewController.h
//  Objective Score Keeper
//
//  Created by Michael Childs on 1/19/16.
//  Copyright © 2016 Michael Childs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>
{
    UILabel* player1Lbl;
    UILabel* player2Lbl;
    
    UIStepper* player2Stepper;
    UIStepper* player1Stepper;
}

@end

