//
//  ViewController.m
//  Objective Score Keeper
//
//  Created by Michael Childs on 1/19/16.
//  Copyright © 2016 Michael Childs. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor darkGrayColor];
    
    UILabel* title = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width / 2.75, 25, 150, 25)];
    title.text = @"Score Keeper";
    [self.view addSubview:title];
    
    int player1Score = 0;
    int player2Score = 0;
    
    
//    player 1
    
    UITextField* player1TF = [[UITextField alloc]initWithFrame:CGRectMake(25, 100, 150, 50)];
    player1TF.placeholder = @"Player 1";
    player1TF.textColor = [UIColor whiteColor];
    player1TF.autocorrectionType = UITextAutocorrectionTypeNo;
    player1TF.returnKeyType = UIReturnKeyDone;
    player1TF.delegate = self;
    player1TF.clearButtonMode = UITextFieldViewModeWhileEditing;
    player1TF.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    player1TF.backgroundColor = [UIColor grayColor];
    player1TF.borderStyle = UITextBorderStyleRoundedRect;
    [self.view addSubview:player1TF];
    
    player1Lbl = [[UILabel alloc] initWithFrame:CGRectMake(player1TF.frame.size.width/2, 150, 150, 100)];
    
    player1Lbl.text = [NSString stringWithFormat:@"%d", player1Score];
    player1Lbl.font = [UIFont systemFontOfSize:70];
    [self.view addSubview:player1Lbl];
    
    player1Stepper = [[UIStepper alloc] initWithFrame:CGRectMake(player1Lbl.frame.origin.x - 25, player1Lbl.frame.origin.y + player1Lbl.frame.size.height, player1Lbl.frame.size.width, 100)];
    player1Stepper.tintColor = [UIColor redColor];
    player1Stepper.maximumValue = 100;
    player1Stepper.minimumValue = 0;
    [player1Stepper addTarget:self action:@selector(player1StepperTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:player1Stepper];
    
    
//    player 2
    
    UITextField* player2TF = [[UITextField alloc]initWithFrame:CGRectMake(player1TF.frame.size.width + 75, 100, 150, 50)];
    player2TF.placeholder = @"Player 2";
    player2TF.textColor = [UIColor whiteColor];
    player2TF.autocorrectionType = UITextAutocorrectionTypeNo;
    player2TF.returnKeyType = UIReturnKeyDone;
    player2TF.delegate = self;
    player2TF.clearButtonMode = UITextFieldViewModeWhileEditing;
    player2TF.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    player2TF.backgroundColor = [UIColor grayColor];
    player2TF.borderStyle = UITextBorderStyleRoundedRect;
    [self.view addSubview:player2TF];
    
    
    player2Lbl = [[UILabel alloc] initWithFrame:CGRectMake(player2TF.frame.origin.x + 50, 150, 150, 100)];
    
    player2Lbl.text = [NSString stringWithFormat:@"%d", player2Score];
    player2Lbl.font = [UIFont systemFontOfSize:70];
    [self.view addSubview:player2Lbl];
    
    player2Stepper = [[UIStepper alloc] initWithFrame:CGRectMake(player2Lbl.frame.origin.x - 25, player2Lbl.frame.origin.y + player2Lbl.frame.size.height, player2Lbl.frame.size.width, 100)];
    player2Stepper.tintColor = [UIColor redColor];
    
    [player2Stepper addTarget:self action:@selector(player2StepperTouched:) forControlEvents:UIControlEventTouchUpInside];
    player2Stepper.maximumValue = 100;
    player2Stepper.minimumValue = 0;
    [self.view addSubview:player2Stepper];
    
    UIButton* resetBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    resetBtn.tintColor = [UIColor redColor];
    resetBtn.frame = CGRectMake(0, self.view.frame.size.height - 50, self.view.frame.size.width, 50);
    [resetBtn setTitle:@"Reset Scores" forState:UIControlStateNormal];
    [self.view addSubview:resetBtn];
    [resetBtn addTarget:self action: @selector(resetBtnTouched:) forControlEvents:UIControlEventTouchUpInside];
    
}// end view did load

-(void) resetBtnTouched: (id)sender {
    player1Lbl.text = @"0";
    player2Lbl.text = @"0";
    
    player1Stepper.value = 0;
    player2Stepper.value = 0;
}

-(void) player1StepperTouched:(UIStepper*)playerStepper {
    player1Lbl.text = [NSString stringWithFormat:@"%.f", playerStepper.value];
}

-(void) player2StepperTouched:(UIStepper*)playerStepper {
    player2Lbl.text = [NSString stringWithFormat:@"%.f", playerStepper.value];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
